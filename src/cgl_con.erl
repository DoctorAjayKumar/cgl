%%% @doc
%%% Conway's Game of Life Controller
%%%
%%% This process is a in charge of maintaining the program's state.
%%% @end

-module(cgl_con).
-vsn("0.1.0").
-author("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-copyright("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-license("MIT").

-behavior(gen_server).
-export([tock/0]).
-export([start_link/0, stop/0]).
-export([init/1, terminate/2,
         handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {window = none                :: none | wx:wx_object(),
         cells  = cgl_cells:pentadecathlon() :: cgl_cells:cells()}).

-type state() :: #s{}.



%% Interface
tock() ->
    gen_server:cast(?MODULE, tock).


start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


stop() ->
    gen_server:cast(?MODULE, stop).



%% gen_server
-spec init(none) -> {ok, state()}.
init(none) ->
    {ok, _Pid} = cgl_tix:start_link(),
    ok = cgl_tix:tick(),
    Window = cgl_gui:start_link(),
    State = #s{window=Window},
    Cells = State#s.cells,
    ok = cgl_gui:cells_redraw(Cells),
    {ok, State}.


handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast(stop, State) ->
    ok = log(info, "Received a 'stop' message."),
    {stop, normal, State};
handle_cast(tock, State) ->
    {ok, NewState} = do_tock(State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = tell(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    zx:stop().

%%% internals

-spec do_tock(state()) -> {ok, NewState :: state()}.
do_tock(State = #s{cells=Cells}) ->
    NewCells = cgl_cells:gen(Cells),
    ok = cgl_gui:cells_redraw(Cells),
    ok = cgl_tix:tick(),
    NewState = State#s{cells=NewCells},
    {ok, NewState}.
