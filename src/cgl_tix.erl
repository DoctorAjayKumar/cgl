-module(cgl_tix).



%% behavior
-behavior(gen_server).
-export([tick/0]).
-export([start_link/0, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").



%% types/records
-record(s,
        {delay_ms = 500 :: pos_integer()}).
-type state() :: #s{}.



%% api
tick() ->
    gen_server:cast(?MODULE, tick).


%% interface
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


stop() ->
    gen_server:cast(?MODULE, stop).



%% gen_server
-spec init(none) -> {ok, state()}.
init(none) ->
    State = #s{},
    {ok, State}.


handle_call(Unexpected, From, State) ->
    ok = tell(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast(stop, State) ->
    ok = log(info, "Received a 'stop' message."),
    {stop, normal, State};
handle_cast(tick, State=#s{delay_ms=DelayMS}) ->
    ok = timer:sleep(DelayMS),
    cgl_con:tock(),
    {noreply, State};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = tell(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.
