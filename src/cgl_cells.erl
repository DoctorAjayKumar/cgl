-module(cgl_cells).

-export([blank/0, blank/1]).
-export([enumerate/1]).
-export([shape/1, topology/1]).
-export([rcth/2]).
-export([gen/1, generation/1]).
-export([gens/2, generations/2]).
-export_type([cells/0, shape/0, rc/0]).

-export([xos/1, print/1, strings/1, printgens/2]).
-export([blinker/0, toad/0, beacon/0, pulsar/0, pentadecathlon/0, lwss/0]).

-type cell() :: on | off.
-record(cells,
        {shape            :: shape(),
         arr              :: array:array(cell()),
         topology = torus :: topology()}).
-opaque cells() :: #cells{}.

% add more topologies later. Torus is the simplest that doesn't have stupid
% effects.
-type topology() :: torus.

% Easy step: torus | projective
%
% Nomenclature annoying: klein bottle (two ways to do it, need a good naming
% convention)
%
% Doable but asinine: rectangular topology (all nonexistent cells are dead).
%
% Evil: dynamic allocation (requires redoing the algorithm because can't just
% loop over the array)
%
% Eventually something like
%
%-type topology() :: torus | projective | finite | dynamic | {cylinder,
%   {ident, lr}} | {cylinder, {ident, tb}} | {moebius, {flip, lr}}, {moebius,
%   {flip, tb}}
%
% eventually I'm going to want a sparse addressing system, where it just stores
% a list of the ones that are alive. aahhh that's annoying. toroidal is really
% simple. projective can be added easily. Finite is annoying but doable.
% Dynamic (meaning it creates cells as they are needed) is needed to implement
% the others, and requires reworking all the data structures. You will get
% toroidal and like it.
%
% Will eventually need some zooming feature. Eh, toroidal for now


% shape = {shape, NumRows, NumCols}
-type shape() :: {shape, pos_integer(), pos_integer()}.
% 0-based indexing
-type rc()    :: {rc, idx0(), idx0()}.
-type idx0()  :: non_neg_integer().


-spec enumerate(cells()) -> [{rc(), cell()}].
enumerate(Cells = #cells{shape={shape, NR, NC}}) ->
    MaxR = NR - 1,
    MaxC = NC - 1,
    RC_Cell =
        fun(R0, C0) ->
            RC = {rc, R0, C0},
            Cell = rcth(RC, Cells),
            {RC, Cell}
        end,
    [RC_Cell(R0, C0) || R0 <- lists:seq(0, MaxR), C0 <- lists:seq(0, MaxC)].


-spec shape(cells()) -> shape().
shape(#cells{shape=S}) -> S.


-spec topology(cells()) -> topology().
topology(#cells{topology=T}) -> T.


-spec blank() -> cells().
blank() ->
    blank(default_shape()).


-spec blank(shape()) -> cells().
blank(Shape) ->
    Arr = all_off(Shape),
    #cells{shape=Shape, arr=Arr}.


-spec default_shape() -> shape().
default_shape() -> {shape, 10, 10}.


-spec all_off(shape()) -> array:array(cell()).
all_off({shape, NR, NC}) ->
    Size = NR*NC,
    array:new([{default, off}, {size, Size}, {fixed, true}]).



-spec rc2i0(shape(), rc()) -> idx0().
rc2i0({shape, _NR, NC}, {rc, R0, C0}) ->
    % Ex for shape={2,5}
    %
    % R0=0 | 0 1 2 3 4
    % R0=1 | 5 6 7 8 9
    %
    % So R0*NC gives head of row
    %
    % So (R0*NC) + C0 gives correct index
    (R0*NC) + C0.


-spec rcth(rc(), cells()) -> cell().
rcth(RC, Cells=#cells{shape=Shape, topology=T}) ->
    rcth_unsafe(rcmod(T, RC, Shape), Cells).


% convert row/column to within bounds according to the topology.
-spec rcmod(topology(), rc(), shape()) -> rc().
% if already in bounds, just return it
% max row is NR-1, similar for column
rcmod(_, RC = {rc, R, C}, {shape, NR, NC}) when R>0, R=<NR-1, C>0, C=<NC-1 ->
    RC;
% assuming out of bounds here
rcmod(torus, {rc, R, C}, {shape, NR, NC}) ->
    NewR = modulo(R, NR),
    NewC = modulo(C, NC),
    {rc, NewR, NewC}.


% computes n mod k but always produces a positive number or 0
-spec modulo(pos_integer(), pos_integer()) -> non_neg_integer().
modulo(N, K) ->
    modulo_plus(K, N rem K).

% Remainder calculated correctly
%
% Reason for this is I want modulo(-4, 5) = 1. Whereas -4 rem 5 = -4.
-spec modulo_plus(pos_integer(), integer()) -> non_neg_integer().
modulo_plus(_K, Rem) when Rem >= 0 ->
    Rem;
modulo_plus(K, Rem) when Rem < 0 ->
    Rem + K.


-spec rcth_unsafe(rc(), cells()) -> cell().
% Assume within bounds
rcth_unsafe(RC, #cells{shape=Shape, arr=Arr}) ->
    ArrIdx0 = rc2i0(Shape, RC),
    array:get(ArrIdx0, Arr).


-spec neighbors(cells(), rc()) -> [cell()].
neighbors(Cells, {rc, R, C}) ->
    % just going top left to bottom right
    NeighborAddresses =
        [ {rc, R-1, C-1}
        , {rc, R-1, C}
        , {rc, R-1, C+1}
        , {rc, R, C-1}
        , {rc, R, C+1}
        , {rc, R+1, C-1}
        , {rc, R+1, C}
        , {rc, R+1, C+1}
        ],
    [rcth(Addr, Cells) || Addr <- NeighborAddresses].


-spec num_live_neighbors(cells(), rc()) -> non_neg_integer().
% number of live neighbors
num_live_neighbors(Cells, RC) ->
    Neighbors = neighbors(Cells, RC),
    IsLive =
        fun
            (on)  -> true;
            (off) -> false
        end,
    LiveNeighbors = lists:filter(IsLive, Neighbors),
    length(LiveNeighbors).


-spec fate(cells(), rc()) -> cell().
% will this cell be alive in the next generation?
fate(Cells, RC) ->
    fate_helper(rcth(RC, Cells), num_live_neighbors(Cells, RC)).

% Rules:
%
% 1. Any live cell with two or three live neighbours survives.
% 2. Any dead cell with three live neighbours becomes a live cell.
% 3. All other live cells die in the next generation.
% 4. All other dead cells stay dead.
fate_helper(on,  2) -> on;
fate_helper(on,  3) -> on;
fate_helper(off, 3) -> on;
fate_helper(on,  _) -> off;
fate_helper(off, _) -> off.


-spec generation(cells()) -> cells().
generation(Cells = #cells{shape=Shape={shape, NR, NC}}) ->
    MaxR0 = NR - 1,
    MaxC0 = NC - 1,
    % given a row and column, return its fate as well as the array index
    FateIdx0 =
        fun(R0, C0) ->
            RC = {rc, R0, C0},
            Fate = fate(Cells, RC),
            Idx0 = rc2i0(Shape, RC),
            {{fate, Fate}, {idx0, Idx0}}
        end,
    % information that will be fed to the fold
    Fates = [FateIdx0(R0, C0) || R0 <- lists:seq(0, MaxR0), C0 <- lists:seq(0, MaxC0)],
    % make a new blank array to fold over
    InitNewArr = all_off(Shape),
    % fold using this function
    Populate =
        fun
            % if it's on, turn it on
            ({{fate, on}, {idx0, ArrIdx0}}, ArrAccum) ->
                array:set(ArrIdx0, on, ArrAccum);
            % cells are off by default, so leave it be
            ({{fate, off}, _}, ArrAccum) ->
                ArrAccum
        end,
    % make new array by folding over RCs
    NewArr = lists:foldl(Populate, InitNewArr, Fates),
    % return same cells with new array
    Cells#cells{arr = NewArr}.


-spec generations(cells(), pos_integer()) -> cells().
% fast-forward however many generations
generations(Cells, 1) -> generation(Cells);
generations(Cells, N) ->
    NewCells = generation(Cells),
    NewN = N - 1,
    generations(NewCells, NewN).


% shortcuts
gen(Cells) -> generation(Cells).
gens(Cells, N) -> generations(Cells, N).


-spec xos([[x|o, ...], ...]) -> cells().

% Convert list of [[x, o], [o, x]] to a cells() type
xos(XOs = [Head | _]) ->
    NR = length(XOs),
    NC = length(Head),
    Shape = {shape, NR, NC},
    XOs_flat = lists:flatten(XOs),
    XO_to_OffOn =
        fun
            (x) -> off;
            (o) -> on
        end,
    OnOffs_flatlist = lists:map(XO_to_OffOn, XOs_flat),
    OnOffs_arr = array:from_list(OnOffs_flatlist),
    OnOffs_fixarr = array:fix(OnOffs_arr),
    #cells{shape=Shape, arr=OnOffs_fixarr}.

-spec strings([string(), ...]) -> cells().
% Convert list of ["..O.O", ".O..."] to a cells() type
%
% period means off, O or * means on
strings(XOs = [Head | _]) ->
    NR = length(XOs),
    NC = length(Head),
    Shape = {shape, NR, NC},
    XOs_flat = lists:flatten(XOs),
    XO_to_OffOn =
        fun
            ($.) -> off;
            ($O) -> on;
            ($*) -> on
        end,
    OnOffs_flatlist = lists:map(XO_to_OffOn, XOs_flat),
    OnOffs_arr = array:from_list(OnOffs_flatlist),
    OnOffs_fixarr = array:fix(OnOffs_arr),
    #cells{shape=Shape, arr=OnOffs_fixarr}.


-spec print(cells()) -> ok.
% print cells to stdout
print(Cells = #cells{shape={shape, NR, _NC}}) ->
    MaxR0 = NR - 1,
    R0s = lists:seq(0, MaxR0),
    PrintRow = fun(R0) -> printrow(Cells, R0) end,
    lists:foreach(PrintRow, R0s).


-spec printrow(cells(), idx0()) -> ok.
printrow(Cells = #cells{shape={shape, _NR, NC}}, R0) ->
    MaxC0 = NC - 1,
    C0s = lists:seq(0, MaxC0),
    PrintCell = fun(C0) -> printcell(Cells, R0, C0) end,
    lists:foreach(PrintCell, C0s),
    io:format("~n").


-spec printcell(cells(), idx0(), idx0()) -> ok.
printcell(Cells, R0, C0) ->
    Cell = rcth({rc, R0, C0}, Cells),
    printcellchar(Cell).


-spec printcellchar(cell()) -> ok.
printcellchar(on)  -> io:format("O");
printcellchar(off) -> io:format(".").


-spec printgens(cells(), non_neg_integer()) -> ok.
printgens(Cells, 0) ->
    print(Cells);
printgens(Cells, N) ->
    print(Cells),
    blankln(),
    printgens(generation(Cells), N-1).


blankln() -> io:format("~n").


%%% some basic configurations

-spec blinker() -> cells().
blinker() ->
    strings([".....",
             ".....",
             ".OOO.",
             ".....",
             "....."]).


-spec toad() -> cells().
toad() ->
    strings(["......",
             "......",
             "..OOO.",
             ".OOO..",
             "......",
             "......"]).


-spec beacon() -> cells().
beacon() ->
    strings(["......",
             ".OO...",
             ".OO...",
             "...OO.",
             "...OO.",
             "......"]).


-spec pulsar() -> cells().
pulsar() ->
    strings([".......................",
             ".......................",
             ".......................",
             ".......................",
             ".......................",
             ".......OOO...OOO.......",
             ".......................",
             ".....O....O.O....O.....",
             ".....O....O.O....O.....",
             ".....O....O.O....O.....",
             ".......OOO...OOO.......",
             ".......................",
             ".......OOO...OOO.......",
             ".....O....O.O....O.....",
             ".....O....O.O....O.....",
             ".....O....O.O....O.....",
             ".......................",
             ".......OOO...OOO.......",
             ".......................",
             ".......................",
             ".......................",
             "......................."]).


-spec pentadecathlon() -> cells().
pentadecathlon() ->
    strings(["..........................",
             "..........................",
             "..........................",
             "..........................",
             "..........................",
             "..........................",
             "..........O....O..........",
             "........OO.OOOO.OO........",
             "..........O....O..........",
             "..........................",
             "..........................",
             "..........................",
             "..........................",
             ".........................."]).


-spec lwss() -> cells().
lwss() ->
    strings([
        ".................",
        ".................",
        ".................",
        ".......O..O......",
        "......O..........",
        "......O...O......",
        "......OOOO.......",
        ".................",
        ".................",
        "................."
    ]).
