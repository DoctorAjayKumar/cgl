%%% @doc
%%% Conway's Game of Life Top-level Supervisor
%%%
%%% The very top level supervisor in the system. It only has one service branch: the
%%% "con" (program controller). The con is the 
%%% only be one part of a larger system. Were this a game system, for example, the
%%% item data management service would be a peer, as would a login credential provision
%%% service, game world event handling, and so on.
%%%
%%% See: http://erlang.org/doc/design_principles/applications.html
%%% See: http://zxq9.com/archives/1311
%%% @end

-module(cgl_sup).
-vsn("0.1.0").
-behaviour(supervisor).
-author("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-copyright("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-license("MIT").

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).


-spec init([]) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init([]) ->
    RestartStrategy = {one_for_one, 0, 60},
    Controller = {cgl_con,
                  {cgl_con, start_link, []},
                  permanent,
                  5000,
                  worker,
                  [cgl_con]},
    Children  = [Controller],
    {ok, {RestartStrategy, Children}}.
