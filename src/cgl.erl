%%% @doc
%%% Conway's Game of Life
%%% @end

-module(cgl).
-vsn("0.1.0").
-behavior(application).
-author("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-copyright("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-license("MIT").

-export([start/2, stop/1]).
-export([print/1, printgens/2, gens/2, gen/1, blinker/0, toad/0, beacon/0,
         pulsar/0, pentadecathlon/0, lwss/0]).


-spec start(normal, Args :: term()) -> {ok, pid()}.
%% @private
%% Called by OTP to kick things off. This is for the use of the "application" part of
%% OTP, not to be called by user code.
%%
%% NOTE:
%%   The commented out second argument would come from ebin/cgl.app's 'mod'
%%   section, which is difficult to define dynamically so is not used by default
%%   here (if you need this, you already know how to change it).
%%
%%   Optional runtime arguments passed in at start time can be obtained by calling
%%   zx_daemon:argv/0 anywhere in the body of the program.
%%
%% See: http://erlang.org/doc/apps/kernel/application.html

start(normal, _Args) ->
    cgl_sup:start_link().


-spec stop(term()) -> ok.
%% @private
%% Similar to start/2 above, this is to be called by the "application" part of OTP,
%% not client code. Causes a (hopefully graceful) shutdown of the application.

stop(_State) ->
    ok.


%%% shortcuts
print(Cells)        -> cgl_cells:print(Cells).
printgens(Cells, N) -> cgl_cells:printgens(Cells, N).
gens(Cells, N)      -> cgl_cells:gens(Cells, N).
gen(Cells)          -> cgl_cells:gen(Cells).
blinker()           -> cgl_cells:blinker().
toad()              -> cgl_cells:toad().
beacon()            -> cgl_cells:beacon().
pulsar()            -> cgl_cells:pulsar().
pentadecathlon()    -> cgl_cells:pentadecathlon().
lwss()              -> cgl_cells:lwss().
