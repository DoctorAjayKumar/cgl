%%% @doc
%%% Conway's Game of Life GUI
%%%
%%% This process is responsible for creating the main GUI frame displayed to the user.
%%%
%%% Reference: http://erlang.org/doc/man/wx_object.html
%%% @end

-module(cgl_gui).
-vsn("0.1.0").
-author("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-copyright("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-license("MIT").

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-export([redraw/0, cells/1, cells_redraw/1]).
-export([start_link/0]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").


-record(s,
        {frame   = none      :: none | wxFrame:wxFrame(),
         board   = none      :: none | wxPanel:wxPanel(),
         sprites = sprites() :: sprites(),
         cells   = none      :: none | cgl_cells:cells()}).


-type state() :: #s{}.
-type sprites() :: #{on  := wx:wxBitmap(),
                     off := wx:wxBitmap()}.



%%% Interface functions


redraw() ->
    wx_object:cast(?MODULE, redraw).


-spec cells(cgl_cells:cells()) -> ok.
cells(Cells) ->
    wx_object:cast(?MODULE, {cells, Cells}).


-spec cells_redraw(cgl_cells:cells()) -> ok.
cells_redraw(Cells) ->
    cells(Cells),
    redraw().


%%% Startup Functions

start_link() ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> {wx:wxObject(), state()}.
init(none) ->
    ok = log(info, "GUI starting..."),
    % frame
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, "Conway's game of life"),

    % board
    Board = wxPanel:new(Frame),

    % connect to close window and resize
    ok = wxFrame:connect(Frame, close_window),
    ok = wxFrame:connect(Frame, size),

    % connect to close window
    ok = wxFrame:center(Frame),
    true = wxFrame:show(Frame),

    State = #s{frame=Frame, board=Board},
    {Frame, State}.


-spec sprites() -> sprites().
sprites() ->
    ImgDir = filename:join(zx_daemon:get_home(), "sprites"),
    PNG = [{type, ?wxBITMAP_TYPE_PNG}],
    #{on  => wxBitmap:new(filename:join(ImgDir, "on.png"), PNG),
      off => wxBitmap:new(filename:join(ImgDir, "off.png"), PNG)}.



-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast(redraw, State) ->
    ok = do_redraw(State),
    {noreply, State};
handle_cast({cells, Cells}, State) ->
    NewState = State#s{cells=Cells},
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_event(Event, State) -> {noreply, NewState}
    when Event    :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The wx_object:handle_event/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_event(#wx{event = #wxClose{}}, State = #s{frame = Frame}) ->
    ok = cgl_con:stop(),
    ok = wxWindow:destroy(Frame),
    {noreply, State};
handle_event(#wx{event = #wxSize{}}, State) ->
    ok = do_redraw(State),
    {noreply, State};
handle_event(Event, State) ->
    ok = tell(info, "Unexpected event ~tp State: ~tp~n", [Event, State]),
    {noreply, State}.



code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().


% see: https://gitlab.com/zxq9/erltris/-/blob/589d1c1f6a9cc9ff56541b66a54eccd3709ca2f7/src/et_gui.erl#L79
-spec do_redraw(state()) -> ok.
do_redraw(#s{cells=none}) ->
    %ok = io:format("No cells to draw~n"),
    ok;
do_redraw(#s{board=Board, cells=Cells, sprites=Sprites}) ->
    % set the size of the board
    {shape, NR, NC} = cgl_cells:shape(Cells),
    BoardPx = {NR*30, NC*30},
    ok = wxPanel:setSize(Board, BoardPx),
    % get the thing we're going to enumerate
    Enum = cgl_cells:enumerate(Cells),
    % bracket draw operation in a drawing context
    DC = wxClientDC:new(Board),
    ok = draw_enum(DC, Sprites, Enum),
    ok = wxClientDC:destroy(DC),
    ok.


-spec draw_enum(wx:wxClientDC(), sprites(), [{cgl_cells:rc(), cgl_cells:cell()}]) -> ok.

% end of the list we're good
draw_enum(_, _, []) ->
    ok;
draw_enum(DC, Map = #{on := ImgOn}, [{{rc, X, Y}, on} | Rest]) ->
    draw(DC, ImgOn, coords(X, Y)),
    draw_enum(DC, Map, Rest);
draw_enum(DC, Map = #{off := ImgOff}, [{{rc, X, Y}, off} | Rest]) ->
    draw(DC, ImgOff, coords(X, Y)),
    draw_enum(DC, Map, Rest).


coords(X, Y) ->
    {X*30, Y*30}.

draw(DC, Image, Coords) ->
    wxClientDC:drawBitmap(DC, Image, Coords).
