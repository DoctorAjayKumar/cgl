all: emake

emake:
	erl -make

dialyzer:
	dialyzer --src src

run:
	zx runlocal


test: beacon blinker pulsar toad

beacon: emake
	cd ebin && ./beacon

blinker: emake
	cd ebin && ./blinker

lwss: emake
	cd ebin && ./lwss

pentadecathlon: emake
	cd ebin && ./pentadecathlon

pulsar: emake
	cd ebin && ./pulsar

toad: emake
	cd ebin && ./toad
