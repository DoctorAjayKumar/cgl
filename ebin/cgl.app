{application,cgl,
             [{description,"Classic cellular automata in Erlang"},
              {registered,[]},
              {included_applications,[]},
              {applications,[stdlib,kernel]},
              {vsn,"0.1.0"},
              {modules,[cgl,cgl_con,cgl_gui,cgl_sup]},
              {mod,{cgl,[]}}]}.
