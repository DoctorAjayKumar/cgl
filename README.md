goal: single black square
goal: show array of blocks

```erlang
% draw black square
DC = wxClientDC:new(Board),
ok = io:format("dc is: ~p~n", [DC]),
Image = maps:get(off, sprites()),
ok = wxClientDC:drawBitmap(DC, Image, {0, 0}),
wxClientDC:destroy(DC),
```

